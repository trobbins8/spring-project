import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.exchange.ExchangeApplication;
import com.exchange.core.Account;
import com.exchange.core.ExchangeRate;
import com.exchange.core.MoneyTransaction;
import com.exchange.service.AccountService;
import com.exchange.service.ExchangeRateService;
import com.exchange.service.MoneyTransactionService;

@SpringBootTest(classes=ExchangeApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class ExchangeRateTest {
	
	@Autowired ExchangeRateService exchangeService;
	@Autowired MoneyTransactionService moneyService;
	@Autowired AccountService accountService;

	@Test
	public void printAllTest() {
		exchangeService.getAll().forEach(p->System.out.println(p.getDateOfTransaction()));
	}

	@Test
	public void moneyTransactionExchangeRateTest() {
		System.out.println("Money Transaction rate test");
		//gets first record from accounts (fred flinstone)
		Account a = accountService.getById(1L).get();
		//create a new monetary transaction adding fred flinstone in the constructor
		MoneyTransaction m = new MoneyTransaction("USD","MEX",1.0,"1/2/2010",a);
		m.setRate(exchangeService.getLastRate(m.getCurrencyFrom(), m.getCurrencyTo()));
		m.setAmountTo(m.getAmountFrom()*m.getRate());
		System.out.println(m.getRate());
	}
	
	@Test
	public void addExchangeRateTest() {
		System.out.println("add 20.0 usd mex exchange rate");
		ExchangeRate er = new ExchangeRate("USD","MEX",20.0);
		exchangeService.add(er);
		Account a = accountService.getById(1L).get();
		MoneyTransaction m = new MoneyTransaction("USD","MEX",1.0,"2/28/2019",a);
		m.setRate(exchangeService.getLastRate(m.getCurrencyFrom(), m.getCurrencyTo()));
		m.setAmountTo(m.getAmountFrom()*m.getRate());
		System.out.println(m.getDate());
		System.out.println(m.getRate());
	}
	@Test
	public void testHistoricalRates() {
		System.out.println("Historical Rates Test");
		Map<String, Map<String, Double>> map = exchangeService.getHistorialRates("USD");
		assertThat(map.entrySet().size(), equalTo(3));
		map.entrySet().stream().forEach(e-> {
			System.out.println(e.getKey());
			e.getValue().entrySet().stream().forEach(p-> System.out.printf("%4s %5.2f %n",p.getKey(), p.getValue()));
		});
	}
}
