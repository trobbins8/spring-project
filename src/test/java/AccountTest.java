import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

import com.exchange.ExchangeApplication;
import com.exchange.controllers.to.AccountTO;
import com.exchange.core.Account;
import com.exchange.service.AccountService;

@SpringBootTest(classes=ExchangeApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountTest {
	
	@Autowired private AccountService service;

	@Test
	public void getNameByID() {
		Account a = service.getById(1L).get();
		assertTrue(a.getFirstName().equals("Fred"));
		assertTrue(a.getLastName().equals("Flintstone"));
	}
	
	@Test
	public void insertTest() {
		System.out.println("insert john jackson");
		Account a = new Account("John","Jackson","Email","PayPal","10/3/2019","USA");
		//adds a new account with name john jackson and prints the first name "john"
		System.out.println(service.getById(service.add(a)).get().getFirstName());
	}
	
	@Test
	public void getAll() {
		System.out.println("print all");
		//prints a list of all accounts, this should jsut be fred flinstone
		service.getAll().forEach(p->System.out.println(p.getFirstName()+" " + p.getLastName()));
	}
	
	@Test
	public void testAddAccount() {
		AccountTO to = new AccountTO();
		to.setFirstName("Barney");
		to.setLastName("Rubble");
		to.setDateOFBirth("1988-12-21");
		to.setContactEmail("barney@gmail.com");
		to.setPayPalEmail("barney@yahoo.com");
		to.setCountryOfResidence("Canada");
		HttpHeaders headers = new HttpHeaders();
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE); // I accept this from server
		headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);// I am sending this
		HttpEntity<AccountTO> aEntity = new HttpEntity<AccountTO>(to, headers);
		ResponseEntity<String> response = new RestTemplate().postForEntity("http://localhost:8081/exchange/account", aEntity, String.class);
		System.out.println(response);
	}

	
}
