import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.format.number.money.MonetaryAmountFormatter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.exchange.ExchangeApplication;
import com.exchange.core.Account;
import com.exchange.core.MoneyTransaction;
import com.exchange.service.AccountService;
import com.exchange.service.MoneyTransactionService;

@SpringBootTest(classes=ExchangeApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class MoneyTransactionTest {

	@Autowired private MoneyTransactionService monetaryService;
	@Autowired private AccountService accountService;
	
	@Test
	public void getAllTest() {
		System.out.println("print all transaction (amount from)");
		monetaryService.getAll().forEach(p->System.out.println(p.getAmountFrom()));
	}
	
	@Test
	public void addTransactionToAccountOne() {
		System.out.println("add transaction");
		//get account for id 1
		Account a = accountService.getById(1L).get();
		//print the size before adding money transaction
		System.out.println(a.getTransactions().size());
		//add a new transaction to a
		a.addTransaction(new MoneyTransaction("from","to",1.0,"10/1/2019",a));
		//save the updated transaction
		accountService.add(a);
		//print after size to, the size should increase by 1
		System.out.println(accountService.getById(1L).get().getTransactions().size());
	}
	@Test
	public void transactionAffiliationWithAccountTest() {
		System.out.println("account affiliation");
		//get monetary transaction for ID 1
		MoneyTransaction m = monetaryService.getById(1L).get();
		//print first and last name of the account affiliated with the first transaction
		System.out.println(m.getAccount().getFirstName() + " " + m.getAccount().getLastName());
	}
	
	@Test
	public void addMonetaryTransaction() {
		System.out.println("add monetary transaction");
		//gets first record from accounts (fred flinstone)
		Account a = accountService.getById(1L).get();
		//prints the size before
		System.out.println(a.getTransactions().size());
		//create a new monetary transaction adding fred flinstone in the constructor
		MoneyTransaction m = new MoneyTransaction("from","to",1.0,"1/2/2010",a);
		//saves the transaction for the database
		monetaryService.addTransaction(m);
		//a is detached so we need to get it from the database again
		a = accountService.getById(1L).get();
		//prints the size before to make sure that the size increased
		System.out.println(a.getTransactions().size());
		
	}
}
