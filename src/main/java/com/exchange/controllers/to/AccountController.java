package com.exchange.controllers.to;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.exchange.core.Account;
import com.exchange.service.AccountService;

@RestController
@RequestMapping("/account")
public class AccountController {
	@Autowired AccountService as;
	
	@GetMapping(path = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE }) 
	public Account getAll(@PathVariable("id")long id) {
		return as.getById(id).get();
	}
	
	@GetMapping(path = "/", produces = { MediaType.APPLICATION_JSON_VALUE }) 
	public Collection<Account> getAll() {
		return (Collection<Account>) as.getAll();
	}
	@PostMapping(consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE) 
	public ResponseEntity<Object> add(@RequestBody AccountTO a) {
		return ResponseEntity.status(HttpStatus.ACCEPTED).build();
	}
	
}
