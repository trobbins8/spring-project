package com.exchange.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.exchange.core.MoneyTransaction;

@Repository
public interface MoneyTransactionRepository extends PagingAndSortingRepository<MoneyTransaction, Long>{
}
