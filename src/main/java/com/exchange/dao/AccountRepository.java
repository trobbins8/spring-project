package com.exchange.dao;

import java.util.Collection;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import com.exchange.core.Account;

@Repository
public interface AccountRepository extends PagingAndSortingRepository<Account, Long>{
}
