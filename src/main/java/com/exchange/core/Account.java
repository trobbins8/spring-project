package com.exchange.core;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.GeneratorType;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@XmlRootElement
@Entity
@Table(name="Account")
public class Account {
	@Id
	@Column(name="ACCOUNTID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	@Column(name="FIRSTNAME")
	private String firstName;
	@Column(name="LASTNAME")
	private String lastName;
	@Column(name="CONTACT_EMAIL")
	private String contactEmail;
	@Column(name="PAYPALL_EMAIL")
	private String payPalEmail;
	@Column(name="DATEOFBIRTH")
	private String dateOfBirth;
	@Column(name="RESIDENCE")
	private String countryOfResidence;
	
	@OneToMany(fetch = FetchType.EAGER,mappedBy="account", cascade = CascadeType.ALL)
	@JsonManagedReference
	private Collection<MoneyTransaction> transactions = new ArrayList<>();
	
	public void addTransaction(MoneyTransaction m) {
		transactions.add(m);
	}
	
	public void setTransactions(Collection<MoneyTransaction> transactions) {
		this.transactions = transactions;
	}

	public Collection<MoneyTransaction> getTransactions(){
		return transactions;
	}
	
	public Account() {
		
	};
	public Account(String firstName, String lastName, String contactEmail, String payPalEmail,
			String dateOfBirth, String countryOfResidence) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.contactEmail = contactEmail;
		this.payPalEmail = payPalEmail;
		this.dateOfBirth = dateOfBirth;
		this.countryOfResidence = countryOfResidence;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getContactEmail() {
		return contactEmail;
	}
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}
	public String getPayPalEmail() {
		return payPalEmail;
	}
	public void setPayPalEmail(String payPalEmail) {
		this.payPalEmail = payPalEmail;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getCountryOfResidence() {
		return countryOfResidence;
	}
	public void setCountryOfResidence(String countryOfResidence) {
		this.countryOfResidence = countryOfResidence;
	}
	
	
}
