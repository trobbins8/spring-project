package com.exchange.core;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonBackReference;

@XmlRootElement
@Entity
@Table(name="MONEYTRANSACTION")
public class MoneyTransaction {

	public Account getAccount() {
		return account;
	}
	public void setAccount(Account account) {
		this.account = account;
	}
	@Id
    @Column(name="TRANID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
    @Column(name="Currency_From")
    private String currencyFrom;
    @Column(name="Currency_To")
    private String currencyTo;
    @Column(name="RATE")
    private double rate;
    @Column(name="Amount_From")
    private double amountFrom;
    @Column(name="Amount_To")
    private double amountTo;
    @Column(name="TRANDATE")
    private String date;
    @ManyToOne
    @JoinColumn(name="ACCOUNTID")
    @JsonBackReference
    private Account account;
    public MoneyTransaction() {
    	
    };
    public MoneyTransaction(String currencyFrom, String currencyTo, double amountFrom,
			 String date, Account account) {
		super();
		this.currencyFrom = currencyFrom;
		this.currencyTo = currencyTo;
		this.amountFrom = amountFrom;
		this.date = date;
		this.account = account;
	}

	public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getCurrencyFrom() {
        return currencyFrom;
    }
    public void setCurrencyFrom(String currencyFrom) {
        this.currencyFrom = currencyFrom;
    }
    public String getCurrencyTo() {
        return currencyTo;
    }
    public void setCurrencyTo(String currencyTo) {
        this.currencyTo = currencyTo;
    }
    public double getRate() {
        return rate;
    }
    public void setRate(double rate) {
        this.rate = rate;
    }
    public double getAmountFrom() {
        return amountFrom;
    }
    public void setAmountFrom(double amountFrom) {
        this.amountFrom = amountFrom;
    }
    public double getAmountTo() {
        return amountTo;
    }
    public void setAmountTo(double amountTo) {
        this.amountTo = amountTo;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    
    
}