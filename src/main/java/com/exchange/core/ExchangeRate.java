package com.exchange.core;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name="EXCHANGE_RATE")
public class ExchangeRate {
	@Id
	@Column(name="RATEID")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	@Column(name="SYMBOL_FROM")
	private String currencyFrom;
	@Column(name="SYMBOL_TO")
	private String currencyTo;
	@Column(name="DATE_OF_TRAN")
	private String dateOfTransaction;
	@Column(name="RATE")
	private double rate;
	
	public ExchangeRate(String currencyFrom, String currencyTo,double rate) {
		super();
		this.currencyFrom = currencyFrom;
		this.currencyTo = currencyTo;
		LocalDateTime dt =LocalDateTime.now().withYear(Integer.parseInt("2019"));
		this.dateOfTransaction = dt.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
		this.rate=rate;
	}
	
	public ExchangeRate() {
		super();
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getCurrencyFrom() {
		return currencyFrom;
	}
	public void setCurrencyFrom(String currencyFrom) {
		this.currencyFrom = currencyFrom;
	}
	public String getCurrencyTo() {
		return currencyTo;
	}
	public void setCurrencyTo(String currencyTo) {
		this.currencyTo = currencyTo;
	}
	public String getDateOfTransaction() {
		return dateOfTransaction;
	}
	public void setDateOfTransaction(String dateOfTransaction) {
		this.dateOfTransaction = dateOfTransaction;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	
}
