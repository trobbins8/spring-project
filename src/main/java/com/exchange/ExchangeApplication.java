package com.exchange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.exchange.service.AccountService;

@SpringBootApplication
public class ExchangeApplication {

	public static void main(String[] args) {
 		SpringApplication app = new SpringApplication(ExchangeApplication.class);
 		app.run(args);
	}

}

