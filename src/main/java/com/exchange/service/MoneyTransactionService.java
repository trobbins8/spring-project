package com.exchange.service;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exchange.core.Account;
import com.exchange.core.MoneyTransaction;
import com.exchange.dao.AccountRepository;
import com.exchange.dao.MoneyTransactionRepository;

@Service
public class MoneyTransactionService {
	@Autowired
	private MoneyTransactionRepository moneyTransactionRepository;
	
	
	public Iterable<MoneyTransaction> getAll(){
		return moneyTransactionRepository.findAll();
	}
	
	@Transactional
	public Long addTransaction(MoneyTransaction tran) {
		moneyTransactionRepository.save(tran);
		return tran.getId();
	}
	
	public Optional<MoneyTransaction> getById(long id){
		return moneyTransactionRepository.findById(id);
	}
}
