package com.exchange.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exchange.core.Account;
import com.exchange.core.MoneyTransaction;
import com.exchange.dao.AccountRepository;

@Service
public class AccountService {
	@Autowired
	private AccountRepository accountRepository;
	
	public Iterable<Account> getAll(){
		return accountRepository.findAll();
	}
	
	public Optional<Account> getById(long id){
		return accountRepository.findById(id);
	}
	
	public Long add(Account a) {
		return accountRepository.save(a).getId();
	}
}
