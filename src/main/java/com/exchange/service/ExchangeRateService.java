package com.exchange.service;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exchange.controllers.to.ExchangeRateTO;
import com.exchange.core.ExchangeRate;
import com.exchange.dao.ExchangeRateRepository;

@Service
public class ExchangeRateService {
	@Autowired ExchangeRateRepository exchangeRepository;
	
	public Iterable<ExchangeRate> getAll(){
		return exchangeRepository.findAll();
	}
	
	public double getLastRate(String from, String to) {
		Optional<ExchangeRate> e = exchangeRepository.findByCurrencyFromAndCurrencyTo(from, to).stream()
		.max(Comparator.comparing(ExchangeRate::getId));
		return e.get().getRate();
	}
	
	public double getHistoricalData(String from, String to) {
		Optional<ExchangeRate> e = exchangeRepository.findByCurrencyFromAndCurrencyTo(from, to).stream()
		.max(Comparator.comparing(ExchangeRate::getId));
		return e.get().getRate();
	}
	
	public long add(ExchangeRate er) {
		return exchangeRepository.save(er).getId();
	}
	public Map<String, Map<String, Double>> getHistorialRates(String from){
		Collection<ExchangeRate> er = exchangeRepository.findByCurrencyFrom(from);
		Map<String, Map<String, Double>> map =  er.stream().map(p-> {
            ExchangeRateTO to = new ExchangeRateTO();
            to.setRate(p.getRate());
            LocalDate localDate = LocalDate.parse(p.getDateOfTransaction().substring(0, 10));
            to.setYear(new Integer(localDate.getYear()).toString());
            to.setCurrencyTo(p.getCurrencyTo());
            return to;
        })
        .sorted((x,y)-> x.getYear().compareTo(y.getYear()))
        .collect(Collectors.groupingBy(ExchangeRateTO::getCurrencyTo, Collectors.groupingBy(ExchangeRateTO::getYear, Collectors.averagingDouble(p-> p.getRate()))));      
       return map;
				
	}
}
